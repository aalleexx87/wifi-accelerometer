#include "network.h"
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
/*
void server_setup() {
  WiFi.config(IPAddress(192, 168, 0, 205), IPAddress(192, 168, 0, 1), IPAddress(255, 255, 255, 0));
  WiFi.begin("internety", "kermit8709");

  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
    digitalWrite(D4, !digitalRead(D4));
  }

  server.begin();
  server.setNoDelay(true);
  Serial.printf("Web server started: %s", WiFi.localIP().toString().c_str());
}

void check_clients() {
  uint8_t connected_clients = 0;
  for(auto &client : clients) {
    if (client) {
      client.println("ping");
      uint8_t retry = 0;
      while(!client.available() && 10 < retry++) {
        delay(50);
      }

      if (retry >= 10) {
        client.stop();
        client = server.available();
        if(client) { connected_clients++; }
      } else {
        client.flush();
        connected_clients++;
      }
    }
  }
  Serial.print("Connected clients: ");
  Serial.println(connected_clients);
}*/

WiFiUDP Udp;

void wifi_setup() {
  WiFi.begin("internety", "kermit8709");

  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
    digitalWrite(D4, !digitalRead(D4));
  }

  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);
}

void send_udp_packet(double* buffer, size_t size) {
    Udp.beginPacket({192,168,0,15}, 2390);
    Udp.write((uint8_t*)buffer, size);
    Udp.endPacket();
  }

  // Udp.beginPacket(Udp.remoteIP(), Udp.remotePort());
  // Udp.write(replyPacekt);
  // Udp.endPacket();
