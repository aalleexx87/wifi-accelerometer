#include <Arduino.h>
#include <MPU9250.h>

#include "network.h"
#include "gyro.h"

ADC_MODE(ADC_TOUT);

// Pin definitions
int myLed  = D4;  // Set up pin 13 led for toggling

MPU9250 myIMU;

void setup()
{
  // TWBR = 12;  // 400 kbit/sec I2C speed
  Serial.begin(115200);

  pinMode(A0, INPUT);

  pinMode(myLed, OUTPUT);
  digitalWrite(myLed, HIGH);

  wifi_setup();
  setup_mpu(myIMU);

  delay(1000);
}

void loop()
{
  double buffer[6];
  if(refresh_mpu_raw(myIMU)) {
    digitalWrite(myLed, !digitalRead(myLed));  // toggle led

    buffer[0] = (double)myIMU.ax;
    buffer[1] = (double)myIMU.ay;
    buffer[2] = (double)myIMU.az;

    buffer[3] = (double)myIMU.gx;
    buffer[4] = (double)myIMU.gy;
    buffer[5] = (double)myIMU.gz;
    //
    // buffer[6] = (double)myIMU.mx;
    // buffer[7] = (double)myIMU.my;
    // buffer[8] = (double)myIMU.mz;
    //
    // buffer[9] = (double)myIMU.temperature;
    // buffer[10] = (double)(analogRead(A0))/74.5;

    send_udp_packet(buffer, sizeof(double[11]));
  }
}
