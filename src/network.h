#ifndef NETWORK_H
#define NETWORK_H

#include <Arduino.h>

//void server_setup();
//void check_clients();
//void publishData(const char* buff);

void wifi_setup();
void send_udp_packet(double* buffer, size_t size);

#endif //NETWORK_H
