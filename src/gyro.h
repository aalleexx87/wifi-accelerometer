#ifndef GYRO_H
#define GYRO_H

#include <MPU9250.h>

void setup_mpu(MPU9250& myIMU);
bool refresh_mpu(MPU9250& myIMU);
bool refresh_mpu_raw(MPU9250& myIMU);

#endif //GYRO_H
