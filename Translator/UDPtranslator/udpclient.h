#ifndef UDPClient_H
#define UDPClient_H

#include <QObject>
#include <QUdpSocket>

class UDPClient : public QObject {
  Q_OBJECT public:

  explicit UDPClient(QObject *parent = 0);

signals:

  void dataAvailable(double* array, size_t size);

public slots:
  void readyRead();

private:
  QUdpSocket *socket;
};
#endif // MYUDP_H
