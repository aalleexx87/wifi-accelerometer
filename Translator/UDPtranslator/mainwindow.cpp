#include <QtWidgets>
#include "mainwindow.h"
#include "ui_mainwindow.h"


#include "MadgwickAHRS/MadgwickAHRS.h"


MainWindow::MainWindow(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::MainWindow),
  _calibrating(false),
  _magnetometerMeans({0,0,0}),
  _magnetometerSamples(0)
{
  ui->setupUi(this);
  ui->progressBar->hide();

  connect(ui->btnCalibrate, &QPushButton::clicked, this, &MainWindow::calibrationStart);
}

MainWindow::~MainWindow()
{
  delete ui;
}

void MainWindow::calibrationStart() {
  ui->btnCalibrate->hide();
  ui->progressBar->show();
  ui->progressBar->setValue(0);

  _magnetometerMeans[0] = 0;
  _magnetometerMeans[1] = 0;
  _magnetometerMeans[2] = 0;
  _magnetometerSamples = 0;
  _calibrating = true;
}

#define CALIBRATION_SAMPLES 2000

void MainWindow::onDataAvailable(double* array, size_t size)
{
  ui->lcdAX->display(array[0]);
  ui->lcdAY->display(array[1]);
  ui->lcdAZ->display(array[2]);

  ui->lcdGX->display(array[3]);
  ui->lcdGY->display(array[4]);
  ui->lcdGZ->display(array[5]);

  ui->lcdMX->display(array[6] - _magnetometerMeans[0]);
  ui->lcdMY->display(array[7] - _magnetometerMeans[1]);
  ui->lcdMZ->display(array[8] - _magnetometerMeans[2]);

  ui->lcdTemp->display(array[9]);
  ui->lcdBatt->display(array[10]);

  if(_calibrating) {
    _magnetometerMeans[0] = (_magnetometerMeans[0] + array[6]) / 2;
    _magnetometerMeans[1] = (_magnetometerMeans[1] + array[7]) / 2;
    _magnetometerMeans[2] = (_magnetometerMeans[2] + array[8]) / 2;

    ui->progressBar->setValue(1000 * _magnetometerSamples / CALIBRATION_SAMPLES);
    if(++_magnetometerSamples >= CALIBRATION_SAMPLES) {
      _calibrating = false;

      ui->progressBar->hide();
      ui->btnCalibrate->show();
    }
  }

  MadgwickAHRSupdate(array[3], array[4], array[5], array[0], array[1], array[2],  array[6], array[7], array[8]);

  _quaternion.setScalar(q0);
  _quaternion.setX(q1);
  _quaternion.setY(q2);
  _quaternion.setZ(q3);

  float roll, pitch, yaw;
  _quaternion.getEulerAngles(&pitch, &yaw, &roll);

  ui->lcdROLL->display(roll);
  ui->lcdPITCH->display(pitch);
  ui->lcdYAW->display(yaw);

}
