#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QQuaternion>


namespace Ui {
  class MainWindow;
}

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  explicit MainWindow(QWidget *parent = 0);
  ~MainWindow();

public slots:
  void onDataAvailable(double* array, size_t size);

private slots:
  void calibrationStart();

private:
  Ui::MainWindow* ui;

  double _magnetometerMeans[3];

  bool _calibrating;
  int _magnetometerSamples;

  QQuaternion _quaternion;
};

#endif // MAINWINDOW_H
