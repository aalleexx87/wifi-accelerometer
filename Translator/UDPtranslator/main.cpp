#include "mainwindow.h"
#include "udpclient.h"
#include <QApplication>

int main(int argc, char *argv[])
{
  QApplication a(argc, argv);
  UDPClient client;
  MainWindow w;

  w.connect(&client, &UDPClient::dataAvailable, &w, &MainWindow::onDataAvailable);
  w.show();

  return a.exec();
}
