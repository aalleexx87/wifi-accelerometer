QT += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = untitled1
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    udpclient.cpp \
    MadgwickAHRS/MadgwickAHRS.cpp

HEADERS  += mainwindow.h \
    udpclient.h \
    MadgwickAHRS/MadgwickAHRS.h

FORMS    += \
    mainwindow.ui
