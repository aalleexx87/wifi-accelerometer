#include "UDPClient.h"
#include <QProcess>

UDPClient::UDPClient(QObject *parent):
  QObject(parent) {

  socket = new QUdpSocket(this);
  socket->bind(QHostAddress::Any, 1234);

  connect(socket, SIGNAL(readyRead()), this, SLOT(readyRead()));
}

void UDPClient::readyRead()
{
  QByteArray buffer;
  buffer.resize(socket->pendingDatagramSize());

  QHostAddress sender;
  quint16 senderPort;

  socket->readDatagram(buffer.data(), buffer.size(), &sender, &senderPort);

  emit dataAvailable(reinterpret_cast<double*>(buffer.data()), buffer.size());
}

