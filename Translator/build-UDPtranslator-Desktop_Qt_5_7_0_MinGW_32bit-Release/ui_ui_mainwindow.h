/********************************************************************************
** Form generated from reading UI file 'ui_mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_UI_MAINWINDOW_H
#define UI_UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLCDNumber>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QVBoxLayout *verticalLayout;
    QGroupBox *groupBox;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QLCDNumber *lcdAX;
    QLabel *label_2;
    QLCDNumber *lcdAY;
    QLabel *label_3;
    QLCDNumber *lcdAZ;
    QGroupBox *groupBox_2;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_4;
    QLCDNumber *lcdGX;
    QLabel *label_5;
    QLCDNumber *lcdGY;
    QLabel *label_6;
    QLCDNumber *lcdGZ;
    QGroupBox *groupBox_3;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label_9;
    QLCDNumber *lcdMX;
    QLabel *label_7;
    QLCDNumber *lcdMY;
    QLabel *label_8;
    QLCDNumber *lcdMZ;
    QGroupBox *groupBox_4;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_10;
    QLCDNumber *lcdTemp;
    QSpacerItem *horizontalSpacer;
    QLabel *label_11;
    QLCDNumber *lcdBatt;
    QGroupBox *groupBox_5;
    QHBoxLayout *horizontalLayout_5;
    QLabel *label_12;
    QLCDNumber *lcdYAW;
    QLabel *label_13;
    QLCDNumber *lcdPITCH;
    QLabel *label_14;
    QLCDNumber *lcdROLL;

    void setupUi(QWidget *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(486, 414);
        verticalLayout = new QVBoxLayout(MainWindow);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        groupBox = new QGroupBox(MainWindow);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        horizontalLayout = new QHBoxLayout(groupBox);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        label = new QLabel(groupBox);
        label->setObjectName(QStringLiteral("label"));
        label->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout->addWidget(label);

        lcdAX = new QLCDNumber(groupBox);
        lcdAX->setObjectName(QStringLiteral("lcdAX"));
        lcdAX->setSmallDecimalPoint(true);
        lcdAX->setDigitCount(3);
        lcdAX->setSegmentStyle(QLCDNumber::Flat);

        horizontalLayout->addWidget(lcdAX);

        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout->addWidget(label_2);

        lcdAY = new QLCDNumber(groupBox);
        lcdAY->setObjectName(QStringLiteral("lcdAY"));
        lcdAY->setSmallDecimalPoint(true);
        lcdAY->setDigitCount(3);
        lcdAY->setSegmentStyle(QLCDNumber::Flat);

        horizontalLayout->addWidget(lcdAY);

        label_3 = new QLabel(groupBox);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout->addWidget(label_3);

        lcdAZ = new QLCDNumber(groupBox);
        lcdAZ->setObjectName(QStringLiteral("lcdAZ"));
        lcdAZ->setSmallDecimalPoint(true);
        lcdAZ->setDigitCount(3);
        lcdAZ->setSegmentStyle(QLCDNumber::Flat);

        horizontalLayout->addWidget(lcdAZ);

        horizontalLayout->setStretch(0, 1);
        horizontalLayout->setStretch(1, 2);
        horizontalLayout->setStretch(2, 1);
        horizontalLayout->setStretch(3, 2);
        horizontalLayout->setStretch(4, 1);
        horizontalLayout->setStretch(5, 2);

        verticalLayout->addWidget(groupBox);

        groupBox_2 = new QGroupBox(MainWindow);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        horizontalLayout_2 = new QHBoxLayout(groupBox_2);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        label_4 = new QLabel(groupBox_2);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_2->addWidget(label_4);

        lcdGX = new QLCDNumber(groupBox_2);
        lcdGX->setObjectName(QStringLiteral("lcdGX"));
        lcdGX->setSmallDecimalPoint(true);
        lcdGX->setDigitCount(3);
        lcdGX->setSegmentStyle(QLCDNumber::Flat);

        horizontalLayout_2->addWidget(lcdGX);

        label_5 = new QLabel(groupBox_2);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_2->addWidget(label_5);

        lcdGY = new QLCDNumber(groupBox_2);
        lcdGY->setObjectName(QStringLiteral("lcdGY"));
        lcdGY->setSmallDecimalPoint(true);
        lcdGY->setDigitCount(3);
        lcdGY->setSegmentStyle(QLCDNumber::Flat);

        horizontalLayout_2->addWidget(lcdGY);

        label_6 = new QLabel(groupBox_2);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_2->addWidget(label_6);

        lcdGZ = new QLCDNumber(groupBox_2);
        lcdGZ->setObjectName(QStringLiteral("lcdGZ"));
        lcdGZ->setSmallDecimalPoint(true);
        lcdGZ->setDigitCount(3);
        lcdGZ->setSegmentStyle(QLCDNumber::Flat);

        horizontalLayout_2->addWidget(lcdGZ);

        horizontalLayout_2->setStretch(0, 1);
        horizontalLayout_2->setStretch(1, 2);
        horizontalLayout_2->setStretch(2, 1);
        horizontalLayout_2->setStretch(3, 2);
        horizontalLayout_2->setStretch(4, 1);
        horizontalLayout_2->setStretch(5, 2);

        verticalLayout->addWidget(groupBox_2);

        groupBox_3 = new QGroupBox(MainWindow);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));
        horizontalLayout_3 = new QHBoxLayout(groupBox_3);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        label_9 = new QLabel(groupBox_3);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_3->addWidget(label_9);

        lcdMX = new QLCDNumber(groupBox_3);
        lcdMX->setObjectName(QStringLiteral("lcdMX"));
        lcdMX->setSmallDecimalPoint(true);
        lcdMX->setDigitCount(3);
        lcdMX->setSegmentStyle(QLCDNumber::Flat);

        horizontalLayout_3->addWidget(lcdMX);

        label_7 = new QLabel(groupBox_3);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_3->addWidget(label_7);

        lcdMY = new QLCDNumber(groupBox_3);
        lcdMY->setObjectName(QStringLiteral("lcdMY"));
        lcdMY->setSmallDecimalPoint(true);
        lcdMY->setDigitCount(3);
        lcdMY->setSegmentStyle(QLCDNumber::Flat);

        horizontalLayout_3->addWidget(lcdMY);

        label_8 = new QLabel(groupBox_3);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_3->addWidget(label_8);

        lcdMZ = new QLCDNumber(groupBox_3);
        lcdMZ->setObjectName(QStringLiteral("lcdMZ"));
        lcdMZ->setSmallDecimalPoint(true);
        lcdMZ->setDigitCount(3);
        lcdMZ->setSegmentStyle(QLCDNumber::Flat);

        horizontalLayout_3->addWidget(lcdMZ);

        horizontalLayout_3->setStretch(0, 1);
        horizontalLayout_3->setStretch(1, 2);
        horizontalLayout_3->setStretch(2, 1);
        horizontalLayout_3->setStretch(3, 2);
        horizontalLayout_3->setStretch(4, 1);
        horizontalLayout_3->setStretch(5, 2);

        verticalLayout->addWidget(groupBox_3);

        groupBox_4 = new QGroupBox(MainWindow);
        groupBox_4->setObjectName(QStringLiteral("groupBox_4"));
        horizontalLayout_4 = new QHBoxLayout(groupBox_4);
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        label_10 = new QLabel(groupBox_4);
        label_10->setObjectName(QStringLiteral("label_10"));
        label_10->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_4->addWidget(label_10);

        lcdTemp = new QLCDNumber(groupBox_4);
        lcdTemp->setObjectName(QStringLiteral("lcdTemp"));
        lcdTemp->setSmallDecimalPoint(true);
        lcdTemp->setDigitCount(3);
        lcdTemp->setSegmentStyle(QLCDNumber::Flat);

        horizontalLayout_4->addWidget(lcdTemp);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer);

        label_11 = new QLabel(groupBox_4);
        label_11->setObjectName(QStringLiteral("label_11"));
        label_11->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_4->addWidget(label_11);

        lcdBatt = new QLCDNumber(groupBox_4);
        lcdBatt->setObjectName(QStringLiteral("lcdBatt"));
        lcdBatt->setSmallDecimalPoint(true);
        lcdBatt->setDigitCount(3);
        lcdBatt->setSegmentStyle(QLCDNumber::Flat);

        horizontalLayout_4->addWidget(lcdBatt);

        horizontalLayout_4->setStretch(0, 1);
        horizontalLayout_4->setStretch(1, 2);
        horizontalLayout_4->setStretch(2, 3);
        horizontalLayout_4->setStretch(3, 1);
        horizontalLayout_4->setStretch(4, 2);
        lcdMY->raise();
        label_9->raise();
        label_7->raise();
        lcdMX->raise();
        label_10->raise();
        label_11->raise();
        lcdBatt->raise();
        lcdTemp->raise();

        verticalLayout->addWidget(groupBox_4);

        groupBox_5 = new QGroupBox(MainWindow);
        groupBox_5->setObjectName(QStringLiteral("groupBox_5"));
        horizontalLayout_5 = new QHBoxLayout(groupBox_5);
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        label_12 = new QLabel(groupBox_5);
        label_12->setObjectName(QStringLiteral("label_12"));
        label_12->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_5->addWidget(label_12);

        lcdYAW = new QLCDNumber(groupBox_5);
        lcdYAW->setObjectName(QStringLiteral("lcdYAW"));
        lcdYAW->setSmallDecimalPoint(true);
        lcdYAW->setDigitCount(3);
        lcdYAW->setSegmentStyle(QLCDNumber::Flat);

        horizontalLayout_5->addWidget(lcdYAW);

        label_13 = new QLabel(groupBox_5);
        label_13->setObjectName(QStringLiteral("label_13"));
        label_13->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_5->addWidget(label_13);

        lcdPITCH = new QLCDNumber(groupBox_5);
        lcdPITCH->setObjectName(QStringLiteral("lcdPITCH"));
        lcdPITCH->setSmallDecimalPoint(true);
        lcdPITCH->setDigitCount(3);
        lcdPITCH->setSegmentStyle(QLCDNumber::Flat);

        horizontalLayout_5->addWidget(lcdPITCH);

        label_14 = new QLabel(groupBox_5);
        label_14->setObjectName(QStringLiteral("label_14"));
        label_14->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_5->addWidget(label_14);

        lcdROLL = new QLCDNumber(groupBox_5);
        lcdROLL->setObjectName(QStringLiteral("lcdROLL"));
        lcdROLL->setSmallDecimalPoint(true);
        lcdROLL->setDigitCount(3);
        lcdROLL->setSegmentStyle(QLCDNumber::Flat);

        horizontalLayout_5->addWidget(lcdROLL);

        horizontalLayout_5->setStretch(0, 1);
        horizontalLayout_5->setStretch(1, 2);
        horizontalLayout_5->setStretch(2, 1);
        horizontalLayout_5->setStretch(3, 2);
        horizontalLayout_5->setStretch(4, 1);
        horizontalLayout_5->setStretch(5, 2);

        verticalLayout->addWidget(groupBox_5);


        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QWidget *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Form", 0));
        groupBox->setTitle(QApplication::translate("MainWindow", "Accelerometer", 0));
        label->setText(QApplication::translate("MainWindow", "X", 0));
        label_2->setText(QApplication::translate("MainWindow", "Y", 0));
        label_3->setText(QApplication::translate("MainWindow", "Z", 0));
        groupBox_2->setTitle(QApplication::translate("MainWindow", "Gyroscope", 0));
        label_4->setText(QApplication::translate("MainWindow", "X", 0));
        label_5->setText(QApplication::translate("MainWindow", "Y", 0));
        label_6->setText(QApplication::translate("MainWindow", "Z", 0));
        groupBox_3->setTitle(QApplication::translate("MainWindow", "Magnetometer", 0));
        label_9->setText(QApplication::translate("MainWindow", "X", 0));
        label_7->setText(QApplication::translate("MainWindow", "Y", 0));
        label_8->setText(QApplication::translate("MainWindow", "Z", 0));
        groupBox_4->setTitle(QApplication::translate("MainWindow", "Misc", 0));
        label_10->setText(QApplication::translate("MainWindow", "Temp", 0));
        label_11->setText(QApplication::translate("MainWindow", "Battery", 0));
        groupBox_5->setTitle(QApplication::translate("MainWindow", "Output", 0));
        label_12->setText(QApplication::translate("MainWindow", "Yaw", 0));
        label_13->setText(QApplication::translate("MainWindow", "Pitch", 0));
        label_14->setText(QApplication::translate("MainWindow", "Roll", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_UI_MAINWINDOW_H
